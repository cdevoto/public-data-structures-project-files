// date.h
// implementation for the date class

class Date{
  friend ostream& operator<< (ostream &, Date &);

  public:
	Date();
	Date(int, int, int);
	~Date();
	
	int getYear();				// returns the specified information
	int getMonth();
	int getDay();

	void setDate(int, int, int);
	void setYear(int);			// sets the date
	void setMonth(int);
	void setDay(int);

	void printDate();			// prints date
	bool Compare(Date, Date);
	void Display(Date, Date);

  private:
	int year;
	int month;
	int day;

};
