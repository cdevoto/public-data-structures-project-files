// date.cpp
// implementation for the date class

#include <iostream>
using namespace std;
#include "date.h"
#include <vector>
#include <string>

// default constructor
Date::Date(){
	year = 2019;
	month = 1;
	day = 1;
}

// regular constructor
Date::Date(int y, int m, int d){
	year = y;
	month = m;
	day = d;
}

// deconstructor
Date::~Date()
{}

// utility methods
void Date::setDate(int y, int m, int d){
	setYear(y);
	setMonth(m);
	setDay(d);
}

void Date::setYear(int y){
	year = y;
}
void Date::setMonth(int m){
        month = m;
}
void Date::setDay(int d){
        day = d;
}

int Date::getYear(){
	return year;
}
int Date::getMonth(){
	return month;
}
int Date::getDay(){
	return day;
}


void Date::printDate() {

  vector<string> months { "JAN", "FEB", "MAR", "APR", "MAY", "JUNE", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC" };

  cout << year << " " << months[month-1] << " " << day;  
}

bool Date::Compare(Date checkDate, Date expDate){               // returns true if the check date is before expiration date
        bool before;
	string s;

	if(checkDate.year < expDate.year){
                before = true;
        }
        else if(checkDate.year == expDate.year){
                if(checkDate.month < expDate.month){
                        before = true;
                }
                else if(checkDate.month == expDate.month){
                        if(checkDate.day < expDate.day){
                                before =  true;
                        }
			if (checkDate.day == expDate.day){
				before = false;
			}
			if (checkDate.day > expDate.day){
				before = false;
			}
                }
        }
	else{
		before = false;
	}

	return before;
}

void Date::Display(Date checkDate, Date expDate){
	bool before;
	string s;
	before = checkDate.Compare(checkDate, expDate);
	s = (before) ? (" is before the intitial date ") : (" is the same or after the initial date, ");

	cout << "The check date " << checkDate << s << expDate << endl;
}

ostream& operator<< (ostream & os, Date & d)
{
	vector<string> months { "JAN", "FEB", "MAR", "APR", "MAY", "JUNE", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC" };

	os << d.year << " " << months[d.month-1]  << " " << d.day;          // returns date

        return os;
}

