// datemain.cpp
// main driver for the date class


#include <iostream>
using namespace std;
#include "date.h"
#include <vector>
#include <string>

int main()
{
  Date initDate(2019, 1, 23);
  Date checkDate1(2019, 1, 18);
  Date checkDate2(2019, 1, 23);
  Date checkDate3(2019, 1, 25);

  checkDate1.Display(checkDate1, initDate);
  checkDate2.Display(checkDate2, initDate);
  checkDate3.Display(checkDate3, initDate); 

  return 0;
}
